## Création d'un network docker
$ docker create network docker
## Lancement de l'image mysql sur le nom mysql dans le réseau docker avec comme mot de passe root et premier database db 
$ docker run  --net=docker --name mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=db -p=80:5000 -d mysql
## Lancement de l'image phpmyadmin avec comme connexion mysql:db et port 8080
$ docker run --net=docker --name phpmyadmin -d --link mysql:db -p 8080:80 phpmyadmin
