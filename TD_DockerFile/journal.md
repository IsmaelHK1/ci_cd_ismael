## Créer la conf Docker
```
nano Dockerfile

```

## Mettre dans ce Dockerfile : 

```
FROM alpine:latest

COPY source.txt /app/source.txt

RUN tac /app/source.txt > /app/inverted.txt

CMD cat /app/inverted.txt
```

## Lancer le Dockker file

```
docker build -t test
docker run test
```
