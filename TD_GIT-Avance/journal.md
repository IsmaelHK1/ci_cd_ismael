# Partie A

```

mkdir mon_repo
cd mon_repo
git init
git commit -m --allow-empty  "initial empty commit" 
git push
nano commit1.txt
git commit -m  "commit1" 
git push
nano commit2.txt
git commit -m  "commit2" 
git push
git switch -c blue
git push --set-upstream origin blue
git switch -c green
git push --set-upstream origin green
git switch main 
nano commit3.txt
git commit -m  "commit3" 
git push
nano commit4.txt
git commit -m  "commit4" 
git push
nano commit5.txt
git commit -m  "commit5" 
git push
nano commit6.txt
git commit -m  "commit6"
git push
git switch bleu
cat commit1 commit2 > commit1
git commit -m  "commit1.1"
git push
cat commit2 commit3 > commit2
git commit -m  "commit1.2"
git push
cat commit3 commit4 > commit3
git commit -m "commit1.3"
git push
git switch -c violet
cat commit4 commit5 > commit4 
git commit -m "commit3.1"
git push
cat commit5 commit6 > commit5
git commit "commit3.2"
git push
touch commit7
git commit "commit3.3"
git push
git switch blue
cat commit4 commit5 > commit4
git commit -m "commit1.4"
git push
git switch green
cat commit1 commit2 > commit1
git commit -m  "commit2.1"
git push
cat commit2 commit3 > commit2
git commit -m  "commit2.2"
git push
cat commit3 commit4 > commit3
git commit -m "commit2.3"
git push
git switch main 
git pull
git merge green 
git push

```

#Partie B 

````
git switch blue
git log --pretty=format:"%h - %an, %ar : %s"
git rebase "hash:commit1.1"
git push
git stwitch violet
git rebase "hash:commit2.1"
git push
```

#Partie C

````
git switch main 
git merge bleu
git push
git merge violet 
git push
```
