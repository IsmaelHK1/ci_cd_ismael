# Lancement de wallabag
```
$ docker network docker
$ docker run --net=docker --name wallabag-db -e "MYSQL_ROOT_PASSWORD=root" -d mariadb
$ docker run --net=docker --name wallabag --link wallabag-db:wallabag-db -e "MYSQL_ROOT_PASSWORD=root" -e "SYMFONY__ENV__DATABASE_DRIVER=pdo_mysql" -e "SYMFONY__ENV__DATABASE_HOST=wallabag-db" -e "SYMFONY__ENV__DATABASE_PORT=3306" -e "SYMFONY__ENV__DATABASE_NAME=wallabag" -e "SYMFONY__ENV__DATABASE_USER=wallabag" -e "SYMFONY__ENV__DATABASE_PASSWORD=wallapass" -e "SYMFONY__ENV__DATABASE_CHARSET=utf8mb4" -e "SYMFONY__ENV__DOMAIN_NAME=http://localhost" -p 80:80 wallabag/wallabag
```
