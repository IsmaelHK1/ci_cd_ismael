### Créer un fichier docker-compose.yml

```
nano docker-compose.yml

version: '3'
services:
  spacedeck:
    build:
      context: .
      dockerfile: /dockerfile
    ports:
      - "8080:8080"
    environment:
      - DATABASE_URL=postgresql://user:password@db/spacedeck
      - MAILER_URL=smtp://mailcatcher:1025
    volumes:
      - spacedeck:/app

  db:
    image: postgres:latest
    environment:
      - POSTGRES_DB=spacedeck
      - POSTGRES_USER=user
      - POSTGRES_PASSWORD=password

  mailcatcher:
    image: schickling/mailcatcher
    ports:
      - "1080:1080"
    environment:
      - MAILCATCHER_HOSTNAME=my-mailcatcher-host
      - MAILCATCHER_SMTP_PORT=1025


# .env file
DATABASE_URL=postgresql://user:password@db/spacedeck
MAILER_URL=smtp://mailcatcher:1025

# lancer le docker

```