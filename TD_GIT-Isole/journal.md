# Creer un serveur git en local et s'y connecter
### Pour creer le serveur
````
mkdir repo_git
cd repo_git
git init --bare
```
### Dans un autre dossier dans la meme vm
```
git clone repo_git new_repo 
```
### Dans une ordi distant en ssh
```
git clone ssh://user@localhost:2222/home/user/repo_git
```
