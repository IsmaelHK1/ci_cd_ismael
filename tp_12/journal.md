## Créer un docker-compose 
```
nano docker-compose.yaml

version: '3.1'

services:
  db1:
    image: mysql:latest
    container_name: db1
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_DATABASE: db1
      MYSQL_USER: admin
      MYSQL_PASSWORD: admin
    ports:
      - "3306:3306"

  db2:
    image: mysql:latest
    container_name: db2
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_DATABASE: db2
      MYSQL_USER: admin
      MYSQL_PASSWORD: admin
    ports:
      - "3307:3306"

  phpmyadmin:
    image: phpmyadmin/phpmyadmin:latest
    container_name: phpmyadmin
    restart: always
    environment:
      PMA_HOST: db1
      PMA_PORT: 3306
      MYSQL_ROOT_PASSWORD: root_password1
    ports:
      - "8080:80"

```

```
$ docker-compose up -d
```

##aller sur http://localhost:8080 pour vous connecter 

-------

##Creer un Dockerfile

```
$ nano DockerFile
```

## Ajouter au Dockerfile le script

```
FROM ubuntu:20.04

RUN pip3 install mysqlclient

ADD dump_script.sh /dump_script.sh
RUN chmod +x /dump_script.sh

ENV DB_HOST=db1 \
    DB_USER=user1 \
    DB_PASSWORD=user_password1 \
    DB_NAME=database1

CMD ["/dump_script.sh"]
```

##Creer un script de dumb

```
nano dumb_script.sh
mysqldump -h $DB_HOST -u $DB_USER -p$DB_PASSWORD $DB_NAME > /dumped_database.sql
```

##Créer l'image docker puis la lancer 

```
docker build -t dockerImage .
docker run -it --rm --name mysql_dumb dockerImage
```

